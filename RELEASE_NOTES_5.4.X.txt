TeXmacs 5.4.2
=============

 * Fix a bug in texmacs.start under Windows (overloading functions were not loaded).


TeXmacs 5.4.1
=============

 * Fix a function name in TEXMACS/macros/texout.sci.


TeXmacs 5.4.0
=============

 * First version of this toolbox extracted from Scilab source tree.
